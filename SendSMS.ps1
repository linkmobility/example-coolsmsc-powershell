﻿param(
    [Parameter(Mandatory=$true)][string]$APIKEY,
    [Parameter(Mandatory=$true)][string]$Sender,
    [Parameter(Mandatory=$true)][string]$Recipients,
    [Parameter(Mandatory=$true)][string]$Message
)

Import-Module .\PSSendSMS\PSSendSMS.psd1 -DisableNameChecking
Send-SMS -APIKEY $APIKEY -Sender $Sender -Recipients $Recipients -Message $Message -Verbose