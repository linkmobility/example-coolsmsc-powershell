[string]$PSSendSMS_URI = 'https://api.linkmobility.dk/v2/message.json?apikey='


# http://technet.microsoft.com/en-us/library/hh849971.aspx

# Send SMS using coolsmsc
Function Send-SMS
{   <#
    .SYNOPSIS
        Send sms with link mobility rest api
    .PARAMETER APIKEY
        coolsmsc username
    .PARAMETER Sender
        From number
    .PARAMETER Recipients
        To number including country code
    .PARAMETER Message
        The message to send
    .EXAMPLE
        Send-SMS -APIKEY sd98fhwq98hf98asdhf98ashdf -Sender Test -Recipients 4512345678 -Message "hello world" -Verbose
    .NOTES
        Author:            Henrik Nicolaisen

        Changelog:
           1.0             Initial Release
    .LINK
        https://linkmobility.atlassian.net/wiki/display/COOL/API
    #>
    Param(
        [string]$APIKEY,
        [string]$Sender = 'SMS GW',
        [string]$Recipients,
        [string]$Message
    )


    Write-Host "Send-SMS $Sender $Recipients $Message"

    $body = @{
        'message' = @{
            sender = $Sender
            recipients = $Recipients
            message = $Message
        }
    }

    $json = $body | ConvertTo-JSON

    Write-Host "Request REST API with $json"

    Invoke-RestMethod -Method Post -Uri $PSSendSMS_URI$APIKEY -ContentType 'application/json' -Body $json
}